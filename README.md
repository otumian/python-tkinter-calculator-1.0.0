# python tkinter calculator - 1.0.0
URL: 
    https://otumian@bitbucket.org/otumian/python-tkinter-calculator-1.0.0.git

Licence: 


Contribute:
    clone and branch, do not ork in the master branch


Contributors:


BUGS:
    point, '.' , does function as expected
    

Technical documentation:

    Conversions:
        Class name: Pascal case
            Eg: Calculator, Mycalculator
        Function Name: Snake case
            Eg: cal_sum, cancel, run_prog
        Variable Name: Lower case
            Eg: calsum, foofoo, numofnumbers
        
    Key file: tkcalcproject.py

    Functions: calculate, enternumber, back and clean. None takes an argument
        except, enternumber
        calculate : updates the output label with the final result of a calculation
        enternumber: inserts an input into the entry/input field
        back: cleans an input to the left when clicked
        clean: clears all data/input in the entry field and sets counter to 0

    variables and constants: counter, answer, label, obj_val, entry and ans
        counter: initially set to 0. This puts the cursor at the left most side of 
        the entry field. counter is used through out this code to determine where 
        the cursor is.
        answer: this has the result of the calculations done
        label: this is the output label, where you see the answer being displayed
        obj_val: is an argument in the function enter number. It is the input
        entered into the entry field when a button is clicked
        entry: takes entered or values from clicked or pressed buttons as input
        ans: takes the value of answer and inserts it into the entry field for 
        dynamic/countionous calculation. Dynamic Calulation is when you use the value
        from a previous calculation for, yet another calculation
            
    run:
        windows - python tkcalcproject.py
        linux - python3 tkcalcproject.py

    quit:
        Ctrl + z


End user documentation:

    addition - plus sign (+): 
        to add 1 to 3, click on the button with 1 on it, followed by the button
        with plus sign (+) on it and then the button with 3 on it. Finally click on the 
        button with the equal to sign (=) on it. The answer shall be displayed in the 
        output label below the entry field

    subtraction - minus sign (-):
        to subtract 2 from 7, click on the button with 7 on it, followed by the button
        with minus sign (-) on it and then the button with 2 on it. Finally click on the 
        button with the equal to sign (=) on it. The answer shall be displayed in the 
        output label below the entry field

    division - forward slash sign (/):
        to divide 8 by 3, click on the button with 8 on it, followed by the button
        with forward slash sign (/) on it and then the button with 3 on it. Finally click on the 
        button with the equal to sign (=) on it. The answer shall be displayed in the 
        output label below the entry field

    multiplication - asterisk sign (*):
        to add 5 to 4, click on the button with 5 on it, followed by the button
        with asterisk sign (*) on it and then the button with 4 on it. Finally click on the 
        button with the equal to sign (=) on it. The answer shall be displayed in the 
        output label below the entry field

    modulo (remainder) - percentage sign (%):
        to find the remainder when 9 is divided by 4, click on the button with 9 on it, followed 
        by the button with percentage sign (%) on it and then the button with 4 on it. Finally 
        click on the button with the equal to sign (=) on it. The answer shall be displayed in 
        the output label below the entry field

    clean - clean:
        delete a character to the left in the entry field

    clear - C:
        deletes all characters in the entry field

    bracket - ():
        to use (, click on the button with ( on it and to use ), click on the button with ) on 
        it.

    cos, sin, tan, sqrt:
        to use cos, sin, tan or sqrt, let's say you want cos(60), click on cos, cos( will apear. 
        enter the 6 and 0 on the appropriate buttons, corresponding to the digit 6 and 0. Now 
        click on the button with ), to close the bracket



TODO:
    percentage /100
    change text on modulo button to mod
    increase the number of input
    add a terminating (exit) button.



THANK YOU!!


