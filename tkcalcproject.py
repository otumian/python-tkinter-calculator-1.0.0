from tkinter import Tk, mainloop, Button, Entry, Label, END, Frame
from math import sin, cos, tan, sqrt, pi

# to determine where the cursor is or manipulate the cursor
counter = 0

# to hold the values after calculations for dynamic calculations
ans = 0

# updates the label to display the answer after calculation
# calling check_decimal_place(result) to round the decimal result to
# 4 decimal places
def calculate():

    """ updates the label to display the answer after calculation
    calling check_decimal_place(result) to round the decimal result to
    4 decimal places
    supports dynamic calculations """

    global answer
    global label
    global ans
    global counter

    answer = eval(str(entry.get()))

    # dynamic calculation
    ans = answer
    ans = check_decimal_place(ans)
    clear()
    entry.insert(counter, ans)
    counter = len(str(entry.get()))

    label.configure(text='= {}'.format(ans))

    print("calc counter is ", counter)

# round the float type results to 4 decimal places
def check_decimal_place(param):

    """ round the float type results to 4 decimal places
    :param param: input
    :return param: 4dp or param """

    if '.' in str(param):
        index_of_dot = list(str(param)).index('.')
        if len(list(str(param))[index_of_dot:]) > 4:
            return '{}'.format(round(param, 4))
    else:
        return param

# takes input
def enternumber(objval):

    """ takes input and adds 1 to counter"""

    global counter
    global entry

    entry.insert(counter, objval)
    print("input counter is ", counter)
    # counter = len(str(entry.get))
    print("Len of counter: ", len(str(entry.get())))
    counter += len(str(entry.get)) + 1

    


# cleans the number entered backwards by one when the back button is pressed
def delete():

    """ cleans the number entered backwards by one when the back button is pressed """

    global counter
    # when user keeps on pressing back but there is nothing to clean
    # any more input falls behind the cursor to the left cursor
    # these line fixes that

    counter = len(str(entry.get()))
    if counter > 0:
        counter -= 1
    else:
        counter = 0

    entry.delete(counter, END)
    print("del counter is ", counter)

# this clears the entry/input field and sets counter to zero
# when C is clicked and sets answer to zero
def clear():

    """this clears the entry/input field and sets counter to zero
    when AC is clicked and sets answer to zero or clear() is called """

    global counter
    global ans

    counter = 0
    answer = 0
    entry.delete(counter, END)

    label.configure(text='= {}'.format(answer))


    print("clear counter is ", counter)

root = Tk()
root.title("Tkinter Calculator")
# root.geometry("300x250")

# Frame to hold the entry field and the text field
entrylabelframe = Frame()
entrylabelframe.grid(row=0, column=0)

# Frame to hold the buttons
buttonframe = Frame()
buttonframe.grid(row=1, column=0)

# Entry/input field
entry = Entry(entrylabelframe)
entry.focus_set()
entry.pack(side='bottom')
entry.grid(row=0, column=0)

# Back button
buttonback = Button(entrylabelframe, text='DEL', command=delete)
buttonback.grid(row=0, column=1)

# C button
buttonclean = Button(entrylabelframe, text='AC', command=clear)
buttonclean.grid(row=0, column=2)

# output/display label
label = Label(entrylabelframe, text='results: ')
label.grid(row=1, column=0)

# 1 button
button1 = Button(buttonframe, text=1, command=lambda x=1: enternumber(x))
button1.grid(row=2, column=0)

# 2 button
button2 = Button(buttonframe, text=2, command=lambda x=2: enternumber(x))
button2.grid(row=2, column=1)

# 3 button
button3 = Button(buttonframe, text=3, command=lambda x=3: enternumber(x))
button3.grid(row=2, column=2)

# 4 button
button4 = Button(buttonframe, text=4, command=lambda x=4: enternumber(x))
button4.grid(row=2, column=3)


# 5 button
button5 = Button(buttonframe, text=5, command=lambda x=5: enternumber(x))
button5.grid(row=3, column=0)

# 6 button
button6 = Button(buttonframe, text=6, command=lambda x=6: enternumber(x))
button6.grid(row=3, column=1)

# 7 button
button7 = Button(buttonframe, text=7, command=lambda x=7: enternumber(x))
button7.grid(row=3, column=2)

# 8 button
button8 = Button(buttonframe, text=8, command=lambda x=8: enternumber(x))
button8.grid(row=3, column=3)


# 9 button
button9 = Button(buttonframe, text=9, command=lambda x=9: enternumber(x))
button9.grid(row=4, column=0)


# 0 button
button0 = Button(buttonframe, text=0, command=lambda x=0: enternumber(x))
button0.grid(row=4, column=1)

# equal to button
buttonequ = Button(buttonframe, text='=', command=calculate)
buttonequ.grid(row=4, column=2)

# plus/addition button
buttonplus = Button(buttonframe, text='+', command=lambda x='+': enternumber(x))
buttonplus.grid(row=4, column=3)


# subtraction button
buttonminus = Button(buttonframe, text='-', command=lambda x='-': enternumber(x))
buttonminus.grid(row=5, column=0)

# product/times button
buttontimes = Button(buttonframe, text='*', command=lambda x='*': enternumber(x))
buttontimes.grid(row=5, column=1)

# division button
buttondivide = Button(buttonframe, text='/', command=lambda x='/': enternumber(x))
buttondivide.grid(row=5, column=2)

# modulo/remainder button
buttonmod = Button(buttonframe, text='%', command=lambda x='%': enternumber(x))
buttonmod.grid(row=5, column=3)


# sine button
buttonsine = Button(buttonframe, text='sin', command=lambda x='sin(': enternumber(x))
buttonsine.grid(row=6, column=0)

# cos button
buttoncos = Button(buttonframe, text='cos', command=lambda x='cos(': enternumber(x))
buttoncos.grid(row=6, column=1)

# tan button
buttontan = Button(buttonframe, text='tan', command=lambda x='tan(': enternumber(x))
buttontan.grid(row=6, column=2)

# pi button
buttonpi = Button(buttonframe, text='pi', command=lambda x='pi': enternumber(x))
buttonpi.grid(row=6, column=3)


# square root button
buttonsqrt = Button(buttonframe, text='sqrt', command=lambda x='sqrt(': enternumber(x))
buttonsqrt.grid(row=7, column=0)

# bracket open button
buttonbracketopen = Button(buttonframe, text='(', command=lambda x='(': enternumber(x))
buttonbracketopen.grid(row=7, column=1)

# bracket close button
buttonbracketclose = Button(buttonframe, text=')', command=lambda x=')': enternumber(x))
buttonbracketclose.grid(row=7, column=2)

# modulo/remainder button
buttonpoint = Button(buttonframe, text='.', command=lambda x='.': enternumber(x))
buttonpoint.grid(row=7, column=3)


mainloop()